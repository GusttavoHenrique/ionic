Objetivo

Projeto criado para a disciplina de desenvolvimento de aplicações híbridas do curso de especialização 
em desenvolvimento para dispositivos móveis da UFRN.

Descrição da Aplicação

A aplicação mapeia os food trucks da região. Qualquer pessoa poderá cadastrar a localização de um food truck e qualquer pessoa poderá ver todas as localizações cadastradas, que sejam atuais (ou seja, uma localização válida).



Componentes: Gusttavo Silva, Diogo Souto