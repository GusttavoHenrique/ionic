import { Inject, Component } from '@angular/core';
import { NavController, AlertController, ModalController } from 'ionic-angular';
import { TelaCadastro } from '../tela-cadastro/tela-cadastro';
import { FirebaseApp, AngularFire, FirebaseListObservable} from 'angularfire2';

import { AuthService } from '../../providers/auth-service';

import { Geolocation } from '@ionic-native/geolocation';

interface marker {
	lat?: number;
	lng?: number;
	nome?: string;
	descricao?: string;
	horarioInicio?: number;
  horarioFim?: number;
  contato?: string;
  icon?: string;
  old?: boolean;
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  zoom: number = 15;
	lat: number = 0;
  lng: number = 0;
  items: FirebaseListObservable<any>;
  firebase: any;
 
  constructor(
    public navCtrl: NavController, 
    public alertCtrl: AlertController, 
    public modalCtrl: ModalController,
    private auth: AuthService,
    af: AngularFire,
    @Inject(FirebaseApp) firebaseApp: any,
    private geolocation: Geolocation) {

      this.firebase = firebaseApp;
      this.items =  af.database.list('/markers/'+this.auth.uid, {
                        query: {
                          orderByChild: 'old',
                          equalTo: false
                        }
                      });
  }

  ionViewDidLoad(){
    this.geolocation.getCurrentPosition().then(
      location => {
          this.lat = location.coords.latitude;
          this.lng = location.coords.longitude;
      }
    ).catch(err => alert("ERROR " + err.message));
  }

  setMarker(evt) {
    this.presentModal(evt);
  }

  presentModal(evt) {
    let modal = this.modalCtrl.create(TelaCadastro);
    modal.present();
    modal.onDidDismiss(params => {
      if(params && params.marker) {
        params.marker.lat = evt.coords.lat,
        params.marker.lng = evt.coords.lng,
        this.salvar(params.marker);
      }
    });
  }

  salvar(marker): void {
    this.items.push(
      {
        nome: marker.nome, 
        descricao: marker.descricao,
        contato: marker.contato,
        horarioInicio: marker.horarioInicio,
        horarioFim: marker.horarioFim,
        icon: 'assets/img/avatar.png',
        lat: marker.lat,
        lng: marker.lng,
        old: marker.old
      }
//    ).then(
//      this.firebase.storage().ref()
//      .putString(this.imageData, "base64")
//      .then(
//        function(){
//         this.
//        }
//      )
    );
  }
}
