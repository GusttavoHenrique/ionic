import { Component, Inject } from '@angular/core';
import { NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { FirebaseApp, AngularFire, FirebaseListObservable} from 'angularfire2';
import { AuthService } from '../../providers/auth-service';

interface marker {
	lat?: number;
	lng?: number;
	nome?: string;
	descricao?: string;
	horarioInicio?: number;
  horarioFim?: number;
  contato?: string;
  icon?: string;
  old?: boolean;
}

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  items: FirebaseListObservable<any>;
  firebase: any;
  selectedItem: any;

  showOnlyActual = "false";
  limit = 15;
  itemsArray: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController,
    public af: AngularFire,
    private auth: AuthService,
    @Inject(FirebaseApp) firebaseApp: any) {
      
      this.firebase = firebaseApp;
      this.selectedItem = navParams.get('item');
      this.items = af.database.list('/markers/'+this.auth.uid);
  }

  queryTasks(infiniteScroll) {  
      if(this.showOnlyActual == "true") {
        this.items =  this.af.database.list('/markers/'+this.auth.uid, {
                        query: {
                          orderByChild: 'old',
                          equalTo: false,
                          limitToFirst: this.limit
                        }
                      });
                      
      } else {
        this.items =  this.af.database.list('/markers/'+this.auth.uid, {
                        query: {
                          orderByChild: 'position',
                          limitToFirst: this.limit
                        }
                      });
      }

      this.items.subscribe(
        result => { this.itemsArray=result; }
      );
      
      if(infiniteScroll) {
        this.items.subscribe(
          result => infiniteScroll.complete()
        );
      }

      this.limit *= 2;
  }

  itemTapped($event, item) {
    this.presentActionSheet(item);
  }

  presentActionSheet(item) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Ações',
      buttons: [
        {
          text: this.getBtnLocalizacaoActionSheet(item),
          icon: 'create',
          handler: () => {
            this.markAsOldLocalization(item);
          }
        },
        {
          text: 'Apagar',
          icon: 'trash',
          role: 'destructive',
          handler: () => {
            this.delete(item);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: 'close',
          handler: () => {
          }
        }
      ]
    });
    
    actionSheet.present();
  } 

  delete(item) {
    this.items.remove(item);
  }

  markAsOldLocalization(item) {
    this.items.update(item, {old: !item.old});
  }

  getBtnLocalizacaoActionSheet(item){
    if(item.old){
      return "Marcar como Loc. Atual";
    } else{
      return "Marcar como Loc. Antiga";
    }
  }

}
