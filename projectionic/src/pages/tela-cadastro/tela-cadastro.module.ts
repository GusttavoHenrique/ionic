import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TelaCadastro } from './tela-cadastro';

@NgModule({
  declarations: [
    TelaCadastro,
  ],
  imports: [
    IonicPageModule.forChild(TelaCadastro),
  ],
  exports: [
    TelaCadastro
  ]
})
export class TelaCadastroModule {}
