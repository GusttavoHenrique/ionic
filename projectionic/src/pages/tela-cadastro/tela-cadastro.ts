import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';

interface marker {
	lat?: number;
	lng?: number;
	nome?: string;
	descricao?: string;
	horarioInicio?: number;
  horarioFim?: number;
  contato?: string;
  icon?: string;
  old?: boolean;
}

@IonicPage()
@Component({
  selector: 'page-tela-cadastro',
  templateUrl: 'tela-cadastro.html',
})
export class TelaCadastro {
  marker = {lat: 0, lng: 0, nome: "", descricao: "", horarioInicio: "", horarioFim: "", contato: "", icon: "", old: false};
  imageData: string;
  cameraData: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    private camera: Camera) {
  }

  close() {
    this.viewCtrl.dismiss();
  }

  openCamera() {
    let options = {
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: true,
      allowEdit: true //permite chamar um editor logo após tirar a foto
      // targetWidth: 100,
      // targetHeight: 100,
    };

    this.camera.getPicture(options).then((imageData) => {
      this.imageData = imageData;
      this.cameraData = "data:image/jpeg;base64," + imageData;
    }, (err) => {
      alert(err);
    });
  }

  cadastrar() {
    let alert = this.alertCtrl.create({
      title: 'Confirmar Cadastro',
      message: 'Deseja confirmar esta operação?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Salvar',
          handler: () => {
            this.viewCtrl.dismiss({marker: this.marker});
          }
        }
      ]
    });
    alert.present();
  }

}
